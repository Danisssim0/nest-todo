# TODO APP

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## DATABASE
```shell
# start db
$ docker-compose -f ./tools/docker/docker-compose.yml up -d
```
### Migrations
Создание чистой миграции для ручного заполнения
```bash
$ yarn run typeorm migration:create -n <migration-name>
```
Применить миграции
```bash
$ yarn run typeorm migration:run
```
### Seeds
Инициализация пустого сида
```bash
$ yarn run seeds migration:create -n <seed-name>
```

Применить сиды
```bash
$ yarn run seeds migration:run
```