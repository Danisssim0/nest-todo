export class UpdateTodoDto {
  readonly title: string;
  readonly status: boolean;
}
