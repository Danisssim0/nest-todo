import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Param,
  Body,
} from '@nestjs/common';
import { TodoService } from './todo.service';
import { Todo } from '../entities/todo.entity';
import { CreateTodoDto } from '../dto/createTodo.dto';
import {DeleteResult, InsertResult} from 'typeorm';
import { UpdateTodoDto } from '../dto/updateTodo.dto';

@Controller('todo')
export class TodoController {
  constructor(private readonly todoService: TodoService) {}

  @Get()
  getAll(): Promise<Todo[]> {
    return this.todoService.getAll();
  }

  @Get(':id')
  getById(@Param('id') id: number): Promise<Todo> {
    return this.todoService.getById(id);
  }

  @Post()
  createTodo(@Body() todo: CreateTodoDto): Promise<InsertResult> {
    return this.todoService.createTodo(todo);
  }

  @Put(':id')
  update(@Body() todo: UpdateTodoDto, @Param('id') id: number) {
    return this.todoService.update(todo, id);
  }

  @Delete(':id')
  delete(@Param('id') id: number): Promise<DeleteResult> {
    return this.todoService.deleteTodo(id);
  }
}
