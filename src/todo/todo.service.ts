import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import {DeleteResult, Repository} from 'typeorm';
import { Todo } from '../entities/todo.entity';
import { UpdateTodoDto } from '../dto/updateTodo.dto';

@Injectable()
export class TodoService {
  constructor(
    @InjectRepository(Todo) private readonly todoRepository: Repository<Todo>,
  ) {}

  getById(id) {
    return this.todoRepository.findOne(id);
  }

  getAll() {
    return this.todoRepository.find();
  }

  createTodo(todo) {
    return this.todoRepository.insert(todo);
  }

  async update(todo: UpdateTodoDto, id: number) {
    const todoForUpdate = await this.todoRepository.findOne({ where: { id } });
    todoForUpdate.status = todo.status;
    todoForUpdate.title = todo.title;
    await this.todoRepository.save(todoForUpdate);
  }

  deleteTodo(id): Promise<DeleteResult> {
    return this.todoRepository.delete(id);
  }
}
